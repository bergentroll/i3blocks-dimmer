# dimmer
## Description

Without dimmer:

![screenshot](dimmer_off.png)

With dimmer:

![screenshot](dimmer_on.png)

This script is not an independent blocklet and meant to use as filter with any
i3blocks blocklets.

dimmer useful if you want to use colored blocklets, that made for dark bar,
with bar that has light background.

dimmer parses a blocket output, gets strings with hex color codes and reduces
it values to make colors darker.

## Using

To use dimmer you should add it through pipe, eg you may change your
i3blocks.conf:

```[ini]
# Global properties
command=(/usr/lib/i3blocks/$BLOCK_NAME ; echo) | $SCRIPT_DIR/dimmer
```

echo is required for properly support of some scripts, that does not produce
new line symbol on the end of their output (like bandwidth blocklet).

## Options

By default dimmer will reduce color values to 50%. Also dimmer supports custom
brightness values in percents as argument, eg:

```
dimmer 72
```

to get values, resuced to 72%.
